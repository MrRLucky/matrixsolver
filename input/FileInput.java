package input;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.event.ChangeEvent;

import solver.Gui;
import solver.Matrix;
/**
 * Implements a file reader to read text files containing matrices. 
 * @author RJ
 *
 */
public class FileInput implements Input {
	// this instance is used to get the content of the text field containing the file name.
	// and to trigger updates of the text area that draws the Matrix
	private Gui gui;
	// The matrix is stored in this data structure.
	private Matrix m;
	/**
	 * Constructor 
	 * @param gui the graphical user interface
	 * @param m the matrix object
	 */
	public FileInput(Gui gui, Matrix m) {
		this.gui = gui;
		this.m = m;
	}
	/**
	 * Fetches the file from the gui that is used to load the matrix 
	 * @return
	 */
	private String getFileNameFromGui() {
		return gui.getTextFieldContent();
	}	
	
	@Override
	/**
	 * Reads a text file containing a Matrix
	 * the file may only contain float compatible values that are separated by blanks
	 * line breaks are used to detect the end of rows
	 */
	public void fetchInput() {
		m.clearMatrix();
		try {
			String fName = "";
			// get filename from gui
			fName = getFileNameFromGui();
			
			if(fName.equals("")) {
				throw new IOException("Empty file path!");
			}
			
			
			FileReader fr = new FileReader(fName);
			BufferedReader input = new BufferedReader(fr);
			String line;
			int j = 0;
			// Read file line by line
			do
			{
				
				line = input.readLine();
				if(line != null) {
					m.add(new ArrayList<Float>());
					String[] values = line.split("\\s+");
					for (String number : values) {					
						m.getRow(j).add(Float.parseFloat(number));
					}
					j++;
				}
			}while (line != null);
			fr.close();
			input.close();
			// Signal the gui that the matrix has changed
			gui.stateChanged(new ChangeEvent(m));
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		fetchInput();
	}
}
