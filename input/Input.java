package input;
import java.awt.event.ActionListener;
/**
 * Very simple interface that can be used to add import methods for matrices
 * @author RJ
 *
 */
public interface Input extends ActionListener {
	void fetchInput();	
}
