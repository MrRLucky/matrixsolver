package algorithms;

import java.util.ArrayList;

import solver.Matrix;

public class DummyAlgorithm extends Algorithm {
	
	public DummyAlgorithm(Matrix m) {
		super(m);
	}	
	
	@Override
	/**
	 * Uses the Douglas Adams algorithm. 
	 */
	public void solve() {
		ArrayList<Float> result = new ArrayList<>();
		result.add(42.0f);
		m.setResults(result);
	}
	@Override
	/**
	 * return the description for the gui
	 */
	public String getDisplayText() {
		return "Solves Matrix in O(1)";
	}

}
