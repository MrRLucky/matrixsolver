package algorithms;
import java.util.ArrayList;

import solver.Matrix;
/**
 * Solves a system of equations using Gaussian elimination (https://en.wikipedia.org/wiki/Gaussian_elimination) performed on a coefficient Matrix 
 * @author RJ
 *
 */
public class GaussElimination extends Algorithm{
	
	public GaussElimination(Matrix m) {
		super(m);
	}

	/**
	 * Substract rows to generate the row echelon form
	 */
	private void substractRows()
	{
		float fHelp,fHelp2;
		for(int a=0;a<m.getRowCount();a++)
		{

			for(int i=a+1;i<m.getRowCount();i++)
			{	
				fHelp=m.getValueAt(i,a);				
				for(int j = a; j < m.getColumnCount(); j++)
				{	
					fHelp2 = m.getValueAt(i, j) - m.getValueAt(a, j) * fHelp / m.getValueAt(a, a);
					m.setValueAt(i,j,fHelp2);			
				}
			}
		}
	}
	/**
	 * Solves a the unknowns (X0 - Xn) in row echelon form using back-substitution
	 */
	private void backSubstitute()
	{
		float x;
		int length1= m.getRowCount() - 1;
		int length2= m.getColumnCount() - 1;
		ArrayList<Float> xValues = new ArrayList<Float>();
		xValues.add(m.getValueAt(length1, length2) / m.getValueAt(length1,length2-1));


		for(int i = 1; i <= length1; i++)
		{
			x = 0;
			for(int j = 0;j < xValues.size(); j++)	
			{
				x = x + m.getValueAt(length1 - i,length2 - j - 1)*xValues.get(j);
			}
			x = m.getValueAt(length1 - i, length2) - x;

			int counter = 0; 
			while(counter < m.getColumnCount() && m.getValueAt(length1-i,counter) == 0) {
				counter++;
			}
			x = x / m.getValueAt(length1 - i, counter);
			xValues.add(x);	
		}		
		m.setResults(xValues);
	}

	@Override
	/**
	 * Performs the Gaussian elimination algorithm 
	 */
	public void solve() {	
		substractRows();
		backSubstitute();		
	}
	@Override
	/**
	 * return the description for the gui
	 */
	public String getDisplayText() {
		return "Use Gauss-Elimination";
	}
}
