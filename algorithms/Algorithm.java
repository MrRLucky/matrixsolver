package algorithms;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import solver.Matrix;
/**
 * Abstract algorithm class that can be implemented in order to easily add new Algorithms 
 * @author RJ
 *
 */
public abstract class Algorithm implements ActionListener{
	// The Matrix the algorithm is performed on
	protected Matrix m;
	
	/**
	 * returns the descriptions of the implemented algorithm
	 * @return
	 */
	public abstract String getDisplayText();
	
	/**
	 * Constructor
	 * @param m the Matrix the algorithm is performed on
	 */
	public Algorithm (Matrix m) {
		this.m = m;
	}
	/**
	 * Triggered to invoke the computation
	 */
	public void actionPerformed(ActionEvent e) {
		m.clearResults();
	    this.solve();
	    e.getSource();
	 } 
	/**
	 * The entry point to the algorithm
	 */
	public abstract void solve();
	
}
