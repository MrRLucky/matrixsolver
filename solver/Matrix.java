package solver;
import java.util.ArrayList;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
/**
 * Abstraction of a coefficient matrix
 * @author RJ
 *
 */
public class Matrix  {
	// Stores the coefficients
	private ArrayList<ArrayList<Float>> values = new ArrayList<ArrayList<Float>>();
	
	// Stores the algorithm results 
	private ArrayList<Float> results = new ArrayList<Float>();
	
	// listener that can be triggered to redraw gui elements
	private ChangeListener gui;
	
	/**
	 * Sets the gui as a ChangeListener that can be triggert when the textArea displaying the matrix must be updatet 
	 * @param gui
	 */
	public void setGui(ChangeListener gui) {
		this.gui = gui;
	}
	/**
	 * swaps to rows of the matrix
	 * @param r1 first row to swap
	 * @param r2 second row to swap
	 */
	public void swapRows (int r1, int r2) {

		float fHelp;
		for(int j = 0; j < values.get(r1).size(); j++)
		{	
			fHelp=getValueAt(r1,j);	
			setValueAt(r1,j,getValueAt(r2,j));
			setValueAt(r2,j,fHelp);
		}	

	}
	/**
	 * deletes all values of the values object
	 */
	public void clearMatrix () {
		values.clear();
	}
	/**
	 * returns value at position (row, col)
	 * @param row row index
	 * @param col column index
	 * @return value at specified position
	 */
	public Float getValueAt(int row, int col) {
		return values.get(row).get(col);
	}
	/**
	 * sets value at position (row, col)
	 * @param row row index
	 * @param col column index
	 * @param value value to be set
	 */
	public void setValueAt(int row, int col, float value) {
		values.get(row).set(col, value);
	}
	/**
	 * returns the number of columns
	 * @return the number of columns
	 */
	public int getColumnCount() {
		return values.get(0).size();
	}
	/**
	 * returns the number of rows
	 * @return the number of rows
	 */
	public int  getRowCount() {
		return values.size();
	}
	/**
	 * adds a new row to the values object
	 * @param row ArrayList to be used as new row
	 */
	public void add(ArrayList<Float> row) {
		values.add(row);
	}
	/**
	 * 
	 * @param row index of row that is to be returned
	 * @return the row at the specified position
	 */
	public ArrayList<Float> getRow(int row) {		
		return values.get(row);
	}
	/**
	 * prints the values object to the console
	 */
	public void printToConsole() {
		for(int i = 0; i < getRowCount(); i++){
			System.out.println(values.get(i));
		}
	}
	/**
	 * clears the results object
	 */
	public void clearResults() {
		results.clear();
	}
	/**
	 * Getter for the results object
	 * @return the result Object
	 */
	public ArrayList<Float> getResults() {
		return results;
	}
	/**
	 * Setter for the results object
	 * @param the results to be set
	 */
	public void setResults(ArrayList<Float> results) {
		this.results = results;
		gui.stateChanged(new ChangeEvent(this));
	}
}
