package solver;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.event.ChangeListener;

import algorithms.Algorithm;
import algorithms.DummyAlgorithm;
import algorithms.GaussElimination;
import input.FileInput;
/**
 * A Matrix solver with simple Gui.
 * @author RJ
 *
 */
public class Solver {
	/**
	 * Main Entry point
	 * @param args are being ignored
	 */
	public static void main(String[] args) {
		// The matrix the algorithm is performed on
		Matrix m = new Matrix();		
		
		// Create to algorithm instances to choose from 
		Algorithm gauss_solver = new GaussElimination(m);
		Algorithm dummy_solver = new DummyAlgorithm(m);
		
		// Give gui loosely coupled instances of the algorithms
		ActionListener[] gui_observers = {(ActionListener) gauss_solver, (ActionListener) dummy_solver};
		
		// Texts to be shown in gui algorithm dropdown
		String [] displayTexts = {gauss_solver.getDisplayText(), dummy_solver.getDisplayText()};
		
		// Safe displaytexts and listeners in a map for the gui, so it has the information
		// how they are correlated
		Map<String, ActionListener> observerMap = new HashMap<String, ActionListener>();
		for (int i = 0; i < displayTexts.length; i++) {
			observerMap.put(displayTexts[i], gui_observers[i]);
		}
		
		Gui gui = new Gui(observerMap);
		
		// Register filereader
		gui.addInputListener((ActionListener) new FileInput(gui, m));
		
		ChangeListener matrix_observer = gui;
		m.setGui(matrix_observer);		
	}

}
