package solver;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
/**
 * Simple Gui with controls for reading files and triggering the calculation.
 * @author RJ
 *
 */
public class Gui extends JFrame implements ChangeListener, ActionListener 
{
	private static final long serialVersionUID = 1L;
	private Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
	private JPanel panel;
	// Displays the matrix
	private JTextArea ta_matrix;
	// Displays the result
	private JTextArea ta_result;
	// Displays the file load instruction.
	private JLabel label_filename;
	// Textfield containing the filename
	private JTextField tf_filename;
	// invokes the loading of the file
	private JButton btn_loadFile;
	// invokes the algorithm that solves the matrix
	private JButton btn_solve;
	// drop down for choosing the algorithm 
	private JComboBox<String> cb_chooseAlgo;
	// maps the algorithm description to the algorithm listeners 
	private Map<String, ActionListener> observerMap;
	
	public Gui(Map<String, ActionListener> algoObserverMap) {
		
		this.observerMap = algoObserverMap;
		
		// window properties title
		setTitle("Gauß Elimination");
		setSize(d.width/4,d.height/2);
		setLayout(new GridLayout(0,1));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel();
		panel.setLayout(new GridLayout(2, 1));		
		
		// Displays the matrix
		ta_matrix = new JTextArea();
		ta_matrix.setEditable(false);
		ta_matrix.getDocument().addDocumentListener(new DocumentListener() {

	        @Override
	        public void removeUpdate(DocumentEvent e) {}
	        @Override
	        public void insertUpdate(DocumentEvent e) {
	        	btn_solve.setEnabled(true);
	        }
	        @Override
	        public void changedUpdate(DocumentEvent arg0) {}
	    });
		
		// Displays the results
		ta_result = new JTextArea();
		ta_result.setEditable(false);	
		
		// Text field containing the filename		
		tf_filename = new JTextField("matrix");
		label_filename = new JLabel("Please enter a filename without extension.");
		
		btn_loadFile = new JButton("Load File");
		
		btn_solve = new JButton("solve");
		btn_solve.setEnabled(false);
		// Add the algorithm descriptions to the dropdown
		ArrayList<String> algoList = new ArrayList<String>();
		for (Map.Entry<String, ActionListener> entry : algoObserverMap.entrySet()) {
			algoList.add(entry.getKey());
		}		
		cb_chooseAlgo = new JComboBox<String>(algoList.toArray(new String[algoList.size()]));
		
		cb_chooseAlgo.addActionListener(this);
		
		// Make initially chosen algorithm listen to solve button 
		btn_solve.addActionListener(algoObserverMap.get(cb_chooseAlgo.getSelectedItem()));

		JScrollPane scroll = new JScrollPane(ta_matrix);
		
		// Add components to window		
		add(label_filename);
		add(panel);	
		add(scroll);
		
		panel.add(tf_filename);
		panel.add(btn_loadFile);
		
		panel.add(cb_chooseAlgo);
		panel.add(btn_solve);
		add(ta_result);
		
		setVisible(true);
		
	}
	
	/**
	 * Adds a listener to the file load button
	 * @param inputObserver
	 */
	public void addInputListener(ActionListener inputObserver) {
		btn_loadFile.addActionListener(inputObserver);
		btn_loadFile.addActionListener(this);
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	/**
	 * On change of the combobox the listener of the selected algorithm is exclusively added to the solve button.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JComboBox) {
			
			//Remove all actionListeners from Button
			for(ActionListener listener : btn_solve.getActionListeners()) {
				btn_solve.removeActionListener(listener);
			}
			
			JComboBox<String> cb = (JComboBox<String>) e.getSource();
			btn_solve.addActionListener(observerMap.get(cb.getSelectedItem()));
		}
	}
	/**
	 * Triggered when a new Result is set in the matrix object or a new matrix was read from file.
	 * @param e the triggered Event
	 */
	public void stateChanged (ChangeEvent e) {
		if(e.getSource() instanceof Matrix) {
			Matrix m = (Matrix) e.getSource();		
			
			if(m.getResults() != null) {
				updateTA_Matrix(m);
				setResults(m);
			}
			else {
				updateTA_Matrix(m);
			}
		}
	}
	/**
	 * Getter for the filename text field.
	 * @return the text content of the filename field
	 */
	public String getTextFieldContent() {
		return tf_filename.getText();
	}
	/**
	 * Sets the result textArea using the results from the matrix object.
	 * @param m the matrix object
	 */
	public void setResults(Matrix m) {		
		ta_result.setText(m.getResults().toString());
	}
	
	/**
	 * Print matrix to text area.
	 * @param m the matrix object
	 */
	public void updateTA_Matrix(Matrix m)
	{
		ta_matrix.setText("");
		ta_matrix.setColumns(m.getColumnCount());	
		String line,value,sep;
		sep=System.getProperty("line.separator");
		for(int i=0;i<m.getRowCount();i++)
		{
			line="";
			for(int j = 0; j < m.getColumnCount(); j++)
			{
				value = String.valueOf(m.getValueAt(i,j));
				// Add separating blanks
				while(value.length() < 10) value = " " + value;
				
				line += value;
			}
			ta_matrix.append(line.concat(sep));
		}
	}
}
