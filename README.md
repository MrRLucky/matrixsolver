# README #

Jar:

Source / executableJar 

Main:

source / solver / solver.java

### What is this repository for? ###

* A simpler matrix solver with small gui to demonstrate basic Java techniques.

### How do I get set up? ###

* The jar file contains a project file for eclipse.
* There are no special dependencies.
* A files describing a matrix must be either in the same directory as the solver
* or must be referenced using the full path. 
* The file may only contain float compatible values that are separated by blanks.
* Line breaks are used to detect the end of rows.

### Who do I talk to? ###

* robin.luckey@gmx.de